export const mockIntegrationProps = {
  id: 25,
  operating: true,
  googleCloudArtifactRegistryProps: {
    artifactRegistryPath: '/path/to/artifact/registry',
  },
  sections: [],
  fields: [],
};
